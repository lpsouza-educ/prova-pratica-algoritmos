﻿using System;

namespace prova_pratica_algoritmos
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;

            do
            {
                Console.Clear();
                Console.WriteLine(" ____                            _      ");
                Console.WriteLine("|  _ \\ _ __ _____   ____ _    __| | ___ ");
                Console.WriteLine("| |_) | '__/ _ \\ \\ / / _` |  / _` |/ _ \\");
                Console.WriteLine("|  __/| | | (_) \\ V / (_| | | (_| |  __/");
                Console.WriteLine("|_|   |_|  \\___/ \\_/ \\__,_|  \\__,_|\\___|");
                Console.WriteLine("                                        ");
                Console.WriteLine("       _                  _ _                       ");
                Console.WriteLine("  __ _| | __ _  ___  _ __(_) |_ _ __ ___   ___  ___ ");
                Console.WriteLine(" / _` | |/ _` |/ _ \\| '__| | __| '_ ` _ \\ / _ \\/ __|");
                Console.WriteLine("| (_| | | (_| | (_) | |  | | |_| | | | | | (_) \\__ \\");
                Console.WriteLine(" \\__,_|_|\\__, |\\___/|_|  |_|\\__|_| |_| |_|\\___/|___/");
                Console.WriteLine("         |___/                                      ");
                Console.WriteLine("");
                Console.WriteLine("Bem vindo a correção da prova de algoritmos!");
                Console.WriteLine("Este programa contempla todos os 8 exercícios em um programa apenas!");
                Console.WriteLine("");
                Console.Write("Para ver os exercícios, escolha um deles (1 a 8): ");

                int exe = 0;

                if (int.TryParse(Console.ReadLine(), out exe))
                {
                    Console.WriteLine("");
                    Console.Write("Inicializando exercício: ");
                    Console.WriteLine(exe);
                    Console.WriteLine("");

                    if (exe == 1)
                    {
                        Exercicio1.Run();
                    }
                    else if (exe == 2)
                    {
                        Exercicio2.Run();
                    }
                    else if (exe == 3)
                    {
                        Exercicio3.Run();
                    }
                    else if (exe == 4)
                    {
                        Exercicio4.Run();
                    }
                    else if (exe == 5)
                    {
                        Exercicio5.Run();
                    }
                    else if (exe == 6)
                    {
                        Exercicio6.Run();
                    }
                    else if (exe == 7)
                    {
                        Exercicio7.Run();
                    }
                    else if (exe == 8)
                    {
                        Exercicio8.Run();
                    }
                    else
                    {
                        Console.WriteLine("");
                        Console.WriteLine("ERRO: Não existe este exercício! =(");
                    }
                }

                Console.WriteLine("");
                Console.Write("Deseja encerrar o programa? (S ou N) ");
                string exitOption = Console.ReadLine().ToUpper();
                exit = (exitOption == "S") ? true : false;

            } while (!exit);

        }
    }
}
