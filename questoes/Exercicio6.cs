using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio6
    {
        public static void Run()
        {
            // Faça um algoritmo para ler um vetor de 10 números. Após isto, ler mais um número qualquer, calcular e escrever quantas vezes esse número aparece no vetor.

            int length = 10;
            int[] numeros = new int[length];

            for (int i = 0; i < length; i++)
            {
                Console.Write("Digite um número (posição do vetor ");
                Console.Write(i);
                Console.Write("): ");
                numeros[i] = int.Parse(Console.ReadLine());
            }

            Console.Write("Digite um número para verificar quantas vezes ele aparece no vetor: ");
            int numeroCompara = int.Parse(Console.ReadLine());
            int count = 0;

            for (int i = 0; i < length; i++)
            {
                if (numeros[i] == numeroCompara)
                {
                    count = count + 1;
                }
            }

            Console.Write("O número ");
            Console.Write(numeroCompara);
            Console.Write(" apareceu ");
            Console.Write(count);
            Console.WriteLine(" vezes!");
        }
    }
}