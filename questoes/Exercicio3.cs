using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio3
    {
        public static void Run()
        {
            // Ler um valor N e imprimir todos os valores inteiros entre 1 (inclusive) e N (inclusive). Considere que o N será sempre maior que ZERO.

            Console.Write("Digite um número maior que zero: ");
            int numero = int.Parse(Console.ReadLine());

            for (int i = 1; i <= numero; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}