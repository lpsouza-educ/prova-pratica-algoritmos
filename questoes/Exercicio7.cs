using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio7
    {
        public static void Run()
        {
            // Ler as notas da 1a. e 2a. avaliações de um aluno. Calcular a média aritmética simples e escrever uma mensagem que diga se o aluno foi ou não aprovado (considerar que nota igual ou maior que 6 o aluno é aprovado). Escrever também a média calculada.

            Console.Write("Digite a primeira nota: ");
            int nota1 = int.Parse(Console.ReadLine());

            Console.Write("Digite a segunda nota: ");
            int nota2 = int.Parse(Console.ReadLine());

            int media = (nota1 + nota2) / 2;

            Console.Write("O aluno esta ");
            if (media >= 6)
            {
                Console.Write("aprovado");
            }
            else
            {
                Console.Write("reprovado");
            }
            Console.Write(" com sua média: ");
            Console.WriteLine(media);
        }
    }
}