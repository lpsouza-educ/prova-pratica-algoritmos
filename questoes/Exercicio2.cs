using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio2
    {
        public static void Run()
        {
            // As maçãs custam R$ 1,30 cada se forem compradas menos de uma dúzia, e R$ 1,00 se forem compradas pelo menos 12. Escreva um programa que leia o número de maçãs compradas, calcule e escreva o custo total da compra.

            double valorMacaNormal = 1.3;
            double valorMacaDuzia = 1;

            Console.Write("Digite a quantidade de maças que deseja vender: ");
            int quantidadeMaca = int.Parse(Console.ReadLine());
            double valorTotal = 0;

            if (quantidadeMaca < 12)
            {
                valorTotal = quantidadeMaca * valorMacaNormal;                
            }
            else
            {
                valorTotal = quantidadeMaca * valorMacaDuzia;
            }

            Console.Write("Valor total das maças são: ");
            Console.WriteLine(valorTotal);
        }
    }
}