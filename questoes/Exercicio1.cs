using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio1
    {
        public static void Run()
        {
            // Exercício 1: A jornada de trabalho semanal de um funcionário é de 40 horas. O funcionário que trabalhar mais de 40 horas receberá hora extra, cujo cálculo é o valor da hora regular com um acréscimo de 50%. Escreva um algoritmo que leia o número de horas trabalhadas em um mês, o salário por hora e escreva o salário total do funcionário, que deverá ser acrescido das horas extras, caso tenham sido trabalhadas (considere que o mês possua 4 semanas exatas).

            int horasSemanais = 40;
            int semanasMes = 4;

            int totalHorasMensal = horasSemanais * semanasMes;

            Console.Write("Digite o total de horas trabalhadas pelo funcionário: ");
            int totalHorasTrabalhadas = int.Parse(Console.ReadLine());

            Console.Write("Digite o salário hora do funcionário: ");
            double salarioHora = int.Parse(Console.ReadLine());

            double salario = 0;

            int horasExtras = 0;
            double salarioExtra = salarioHora + (salarioHora*0.5);

            if (totalHorasTrabalhadas > totalHorasMensal)
            {
                horasExtras = totalHorasTrabalhadas - totalHorasMensal;
                salario = (totalHorasMensal * salarioHora) + (horasExtras * salarioExtra);
            } else {
                salario = totalHorasTrabalhadas * salarioHora;
            }

            Console.Write("Total de salario a pagar para o funcionário: ");
            Console.WriteLine(salario);
        }
    }
}