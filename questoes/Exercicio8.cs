using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio8
    {
        public static void Run()
        {
            // O custo de um carro novo ao consumidor é a soma do custo de fábrica com a porcentagem do distribuidor e dos impostos (aplicados ao custo de fábrica). Supondo que o percentual do distribuidor seja de 28% e os impostos de 45%, escrever um algoritmo para ler o custo de fábrica de um carro,calcular e escrever o custo final ao consumidor.

            Console.Write("Custo de fábrica do carro: ");
            double custo = double.Parse(Console.ReadLine());

            double distribuidor = custo * 0.28;
            double impostos = custo * 0.45;
            double custoFinal = custo + distribuidor + impostos;

            Console.Write("O custo final do consumidot para este carro é de: R$ ");
            Console.WriteLine(custoFinal);
        }
    }
}