using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio4
    {
        public static void Run()
        {
            // Escreva um algoritmo que calcule e imprima a tabuada do 2 ao 10.

            for (int i = 2; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    int res = i * j;
                    Console.Write(i);
                    Console.Write(" X ");
                    Console.Write(j);
                    Console.Write(" = ");
                    Console.WriteLine(res);
                }
            }
        }
    }
}