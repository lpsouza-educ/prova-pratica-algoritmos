using System;

namespace prova_pratica_algoritmos
{
    public class Exercicio5
    {
        public static void Run()
        {
            // Faça um algoritmo para ler 10 números e armazenar em um vetor. Após isto, o algoritmo deve ordenar os números no vetor em ordem crescente. Escrever o vetor ordenado.

            int length = 10;
            int[] numeros = new int[length];

            for (int i = 0; i < length; i++)
            {
                Console.Write("Digite um número (posição do vetor ");
                Console.Write(i);
                Console.Write("): ");
                numeros[i] = int.Parse(Console.ReadLine());
            }

            int min, aux;

            for (int i = 0; i < length - 1; i++)
            {
                min = i;

                for (int j = i + 1; j < length; j++)
                    if (numeros[j] < numeros[min])
                        min = j;

                if (min != i)
                {
                    aux = numeros[min];
                    numeros[min] = numeros[i];
                    numeros[i] = aux;
                }
            }

            for (int i = 0; i < length; i++)
            {
                Console.WriteLine(numeros[i]);
            }

        }
    }
}