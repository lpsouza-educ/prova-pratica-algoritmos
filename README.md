# Correção da prova de algoritmos

Este é o código fonte do programa que tras as resposta de todas as questões da prova prática de algoritmos!

## Para rodar

1. Baixe o código
2. Tendo o ambiente (.NET Core) para rodar, digite no console:
    * `dotnet run`

## Onde estão os códigos dos exercícios?

Olhem na pasta `questoes` no código fonte.

